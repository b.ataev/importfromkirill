unit People;

interface

uses
  Main1,BigModT, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids;



Type  TTableModT = class(TForm)
    PrepareAspects: TButton;
    GoAspects: TButton;
    ExtrRbt: TRadioButton;
    StringGrid1: TStringGrid;
    IntuitionEd: TEdit;
    LogicEd: TEdit;
    EthicsEd: TEdit;
    SensEd: TEdit;
    procedure GoAspectsClick(Sender: TObject);
    procedure DoAspMetab(var a:Tasp);
    procedure PrepareAspectsClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TableModT: TTableModT;
  Intuit:TAsp;
  Logic: TAsp;
  Don:Thum;


implementation

{$R *.dfm}
//

 procedure TTableModT.DoAspMetab(var a: TAsp);
begin
  IsAspHF(a);
  IsAspAct(a);
  IsAspInAct(a);
end;



procedure TTableModT.PrepareAspectsClick(Sender: TObject);
begin
  Don.createMan();
  Don.ModT[0].Inf.Info :=strtoint(IntuitionEd.text);
  Don.ModT[1].Inf.Info :=strtoint(LogicEd.text);
  Don.ModT[2].Inf.Info :=strtoint(EthicsEd.text);
  Don.ModT[3].Inf.Info :=strtoint(SensEd.text);

end;

procedure TTableModT.GoAspectsClick(Sender: TObject);
begin
  if ExtrRbt.Checked then Main1.extravert:=true;

  DoAspMetab(Don.ModT[0]);
  Stringgrid1.cells[2,1] := Don.PrintTypeIn(Don.ModT[0]) ;
  Stringgrid1.cells[4,1] := Don.PrintTypeIn(Don.ModT[1]) ;
  Stringgrid1.cells[6,1] := Don.PrintTypeIn(Don.ModT[2]) ;
  Stringgrid1.cells[8,1] := Don.PrintTypeIn(Don.ModT[3]) ;
  Stringgrid1.cells[2,4] :=  Don.PrintTypeIEx(Don.ModT[0]) ;
  Stringgrid1.cells[4,4] :=  Don.PrintTypeIEx(Don.ModT[1]) ;
  Stringgrid1.cells[6,4] :=  Don.PrintTypeIEx(Don.ModT[2]) ;
  Stringgrid1.cells[8,4] :=  Don.PrintTypeIEx(Don.ModT[3]) ;
  if Don.ModT[0].active then
  begin
  Stringgrid1.cells[2,2] := '+' ;
  DoAspMetab(Don.ModT[1]);
  end
  else
  Stringgrid1.cells[2,2] := '-' ;

  if Don.ModT[1].active then
  begin
   Stringgrid1.cells[4,2] := '+' ;
  DoAspMetab(Don.ModT[2]);
  end
  else
  Stringgrid1.cells[4,2] := '-' ;

  if Don.ModT[2].active then
  begin
  Stringgrid1.cells[6,2] := '+' ;
  DoAspMetab(Don.ModT[3]);
    end
    else
   Stringgrid1.cells[6,2] := '-' ;

     if Don.ModT[3].active then
  begin
  Stringgrid1.cells[8,2] := '+' ;
  DoAspMetab(Don.ModT[4]);
    end
    else
   Stringgrid1.cells[6,2] := '-' ;

end;

end.
