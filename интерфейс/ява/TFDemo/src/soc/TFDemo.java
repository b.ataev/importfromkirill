package soc;// Use a text field.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TFDemo implements ActionListener {

    static JTextField
            //далее- поля  введения цифр мощности входного потока и номера аспекта для:
            //  интуиции
            jtfExInf_Int, jtfExInf_Int_Numb,
    //логики
    jtfExInf_Log, jtfExInf_Log_Numb,
    // этики
    jtfExInf_Et, jtfExInf_Et_Numb,
    // сенсорики
    jtfExInf_Sen, jtfExInf_Sen_Numb;
    JButton jbtnFirst;
    JLabel
            //далее- label  введения цифр мощности входного потока:
            jlabInfoInt, jlabInfoIntLog, jlabInfoIntEt, jlabInfoIntSen,
    //   является ли инфа аспекта вычокочастотной для:
    jlabIsItHFint, jlabIsItHFlog, jlabIsItHFet, jlabIsItHFsen,
    //   номера аспекта для:
    jlabIsItHFintNumb, jlabIsItHFlogNumb, jlabIsItHFetNumb, jlabIsItHFsenNumb;
    public static soc.Mod_T Don = new soc.Mod_T(true);
    //public Asp As = new Asp();
    //копируем из программы для форм
    JButton btn1 = new JButton("Open new form");
    static JTextField txt1 = new JTextField(3);
    static JTextField txt2 = new JTextField(10);
    JScrollPane scr = new JScrollPane(txt2);

    void FindAspect(String aspname, int Can) {
        int i;
        for (i = 0; i < 4; ++i) {
            if (Don.ArAsp[i].NameAsp == aspname)
                Don.ArAsp[i].Inf.Info = Can;
        }
    }

    ;

    <jtfExInf_Int> TFDemo() {

        // Create a new JFrame container.
        JFrame jfrm = new JFrame("частотнотна ли инфа? если да, то появиться true");

        // Specify FlowLayout for the layout manager.
        jfrm.setLayout(new FlowLayout());

        // Give the frame an initial size.
        jfrm.setSize(510, 900);
        jfrm.setResizable(false);

        // Terminate the program when the user closes the application.
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create a text field.
        jtfExInf_Int = new JTextField(10);
        jtfExInf_Log = new JTextField(10);
        jtfExInf_Et = new JTextField(10);
        jtfExInf_Sen = new JTextField(10);
        jtfExInf_Int_Numb = new JTextField(10);
        jtfExInf_Log_Numb = new JTextField(10);
        jtfExInf_Et_Numb = new JTextField(10);
        jtfExInf_Sen_Numb = new JTextField(10);


        // Set the action commands for the text field.
        jtfExInf_Int.setActionCommand("myTF");

        // Create the Reverse button.
        JButton jbtnFirst = new JButton("рассчёт");


        // Add action listeners.
        jtfExInf_Int.addActionListener(this);
        jbtnFirst.addActionListener(this);

        // Create the labels.
        jlabInfoInt = new JLabel("Ввод масштаба  1-ой инфы: ");
        jlabInfoIntLog = new JLabel("Ввод масштаба  2-ой инфы: ");
        jlabIsItHFint = new JLabel("неизвестная 1-я частота");
        jlabIsItHFlog = new JLabel("неизвестная 2-я частота");
        jlabInfoIntEt = new JLabel("Ввод масштаба  3-ой инфы: ");
        jlabInfoIntSen = new JLabel("Ввод масштаба  4-ой инфы: ");
        jlabIsItHFet = new JLabel("неизвестная 3-я частота");
        jlabIsItHFsen = new JLabel("неизвестная 4-я частота");

        jlabIsItHFintNumb = new JLabel("Ввод номера  интуиции: ");
        jlabIsItHFlogNumb = new JLabel("Ввод номера  логики: ");
        jlabIsItHFetNumb = new JLabel("Ввод номера  этики: ");
        jlabIsItHFsenNumb = new JLabel("Ввод номера  сенсорики: ");


        // Add the components to the content pane.
        jfrm.add(jlabInfoInt);
        jfrm.add(jtfExInf_Int);
        jfrm.add(jlabIsItHFint);
        jfrm.add(jlabInfoIntLog);
        jfrm.add(jtfExInf_Log);
        jfrm.add(jlabIsItHFlog);
        jfrm.add(jlabInfoIntEt);
        jfrm.add(jtfExInf_Et);
        jfrm.add(jlabIsItHFet);
        jfrm.add(jlabInfoIntSen);
        jfrm.add(jtfExInf_Sen);
        jfrm.add(jlabIsItHFsen);

        jfrm.add(jlabIsItHFintNumb);
        jfrm.add(jtfExInf_Int_Numb);
        jfrm.add(jlabIsItHFintNumb);


        jfrm.add(jtfExInf_Log_Numb);
        jfrm.add(jlabIsItHFlogNumb);
        jfrm.add(jtfExInf_Et_Numb);
        jfrm.add(jlabIsItHFetNumb);
        jfrm.add(jtfExInf_Sen_Numb);
        jfrm.add(jlabIsItHFsenNumb);
        jfrm.add(jbtnFirst);


        // Display the frame.
        jfrm.setVisible(true);
        jfrm.add(btn1);
        btn1.addActionListener(this);
        jfrm.add(txt1);
        jfrm.add(txt2);
        scr.setPreferredSize(new Dimension(450, 110));
        jfrm.add(scr, BorderLayout.CENTER);
    }

    // Handle action events.
    public void actionPerformed(ActionEvent e) {
// если нажата кнопка btn1 (new form),то
        if (e.getSource() == btn1) {// открыть новую форму, в которой будет таблица
            form2 formt = new form2(Don);
        }
// если нажата кнопка расчёт
        if (e.getActionCommand().equals("рассчёт")) {
            // The Reverse button was pressed.
            // считать данные с формы в промежуточные краткие переменные
            Integer IntStg = Integer.parseInt(jtfExInf_Int.getText());
            Integer LogStg = Integer.parseInt(jtfExInf_Log.getText());
            Integer EtStg = Integer.parseInt(jtfExInf_Et.getText());
            Integer SensStg = Integer.parseInt(jtfExInf_Sen.getText());
            Integer IntN = Integer.parseInt(jtfExInf_Int_Numb.getText());
            Integer LogN = Integer.parseInt(jtfExInf_Log_Numb.getText());
            Integer EtN = Integer.parseInt(jtfExInf_Et_Numb.getText());
            Integer SensN = Integer.parseInt(jtfExInf_Sen_Numb.getText());
            // считать данные с промежуточных кратких переменных в массивы психики

            Don.ArAsp[0] = new Asp();
            Don.ArAsp[0].Inf = new Inform();
            Don.ArAsp[1] = new Asp();
            Don.ArAsp[1].Inf = new Inform();
            Don.ArAsp[2] = new Asp();
            Don.ArAsp[2].Inf = new Inform();
            Don.ArAsp[3] = new Asp();
            Don.ArAsp[3].Inf = new Inform();

//todo здесь должно быть определение, какой где аспект из дельфи функция FindAspect в InfoInfo
            Don.ArAsp[SensN - 1].NameAsp = "sens";//э
            Don.ArAsp[EtN - 1].NameAsp = "et";//э
            Don.ArAsp[IntN - 1].NameAsp = "int";//э
            Don.ArAsp[LogN - 1].NameAsp = "log";//э
            FindAspect("sens", SensN);
            FindAspect("et", EtN);
            FindAspect("int", IntN);
            FindAspect("log", LogN);

            Demo_Asp_Inf.IsAspHF(Don.ArAsp[0]);
            jlabIsItHFint.setText(String.valueOf(Don.ArAsp[0].Inf.InfIsHF));


        } else
            // Enter was pressed while focus was in the
            // text field.
            jlabIsItHFint.setText("You pressed ENTER. Text is: " +
                    jtfExInf_Int.getText());
    }

    public static void main(String args[]) {
        // Create the frame on the event dispatching thread.
        SwingUtilities.invokeLater(new Runnable() {
            //что такое ран?
            public void run() {
                new TFDemo();

            }
        });
        // Create a new JFrame container.
        // JFrame jfrm = new JFrame("частотнотна ли инфа? если да, то появиться true");

    }
}
